﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class Clock : MonoBehaviour {

	public Transform wskazowkaMinutowa;
	public Transform wskazowkaGodzinowa;
    float degreesPerHour = -30f;
    float degreesPerMinute = -6f;

    public InputField inputHour;
    public InputField inputMinute;
    public Text display;
    public GameObject partSystem;

    int randomHour;
    int randomMinute;
    string randomHourString;
    string randomMinuteString;
    string randomHourStringPM;
    bool inpHourBool = false;
    public Text podpowiedzGodziny;
    public Text podpowiedzMinuty;
    public Text txtTotalPoints;

    int taskNumber;
    public int totalPoints;
    public bool inpHourLength;
    public AudioSource audioS;

    public Transform infoPanel;
    bool isHintShown;
    bool isMorningHour;
    public Text txt_rano_popoludniu;

    [SerializeField] Image panelbg;
    [SerializeField] Sprite[] bgPics;

    private void Awake()
    {
        
    }
    void Start () {
        UpdateThePoints();
        taskNumber = 0;
        totalPoints = 0;
        ShowTheTime();
        inpHourLength = false;
        isHintShown = false;
    }
	
	
	void Update () {

        if (Input.GetKeyDown(KeyCode.Tab)){
            ActivateNextInputField();
        }

        if((inputHour.text.Length == 2) && (inpHourLength == false))
        {
            
            ActivateNextInputField();
            inpHourLength = true;

        }
        if(isMorningHour == true)
        {


            if (inputHour.text == randomHourString && inputMinute.text == randomMinuteString && isHintShown == false)
            {

                Particles(7.6f, 4.3f);
                ShowTheTime();
                GetAPoint();
            }
            else if (inputHour.text == randomHourString && inputMinute.text == randomMinuteString && isHintShown == true)
            {

                Particles(7.6f, 4.3f);
                ShowTheTime();
                UpdateThePoints();
            }
        }
        else
        {
            if (inputHour.text == randomHourStringPM && inputMinute.text == randomMinuteString && isHintShown == false)
            {

                Particles(7.6f, 4.3f);
                ShowTheTime();
                GetAPoint();
            }
            else if (inputHour.text == randomHourStringPM && inputMinute.text == randomMinuteString && isHintShown == true)
            {

                Particles(7.6f, 4.3f);
                ShowTheTime();
                UpdateThePoints();
            }
        }
        
        
       
	}

    void ActivateNextInputField()
    {
        if(inpHourBool == true)
        {
            inputMinute.ActivateInputField();
            inpHourBool = false;
        }
        else
        {
            inputHour.ActivateInputField();
            inpHourBool = true;
        }
        
    }

    void ShowTheTime()
    {
        int morningEveningSwitch = UnityEngine.Random.Range(1, 10);
        if (morningEveningSwitch <= 5)
        {
            isMorningHour = true;
            txt_rano_popoludniu.text = "Rano";
            panelbg.sprite = bgPics[0];
        }
        else
        {
            isMorningHour = false;
            txt_rano_popoludniu.text = "Po południu";
            panelbg.sprite = bgPics[1];
        }

        isHintShown = false;
        inpHourLength = false;
        podpowiedzGodziny.text = "";
        podpowiedzMinuty.text = "";
        ActivateNextInputField();
        inputHour.text = "";
        inputMinute.text = "";
        randomHour = UnityEngine.Random.Range(0, 12);
        randomMinute = UnityEngine.Random.Range(0, 60);
        int addRotationForHour = randomMinute / 2;

        wskazowkaMinutowa.localRotation =
            Quaternion.Euler(0f, 0f, randomMinute * degreesPerMinute);
        wskazowkaGodzinowa.localRotation =
            Quaternion.Euler(0f, 0f, (randomHour * degreesPerHour) - addRotationForHour);

        if (randomHour < 10)
        {
            randomHourString = "0" + randomHour;
            randomHourStringPM = (randomHour + 12).ToString();
        }
        else
        {
            randomHourString = randomHour.ToString();
            randomHourStringPM = (randomHour + 12).ToString();
        }

        if (randomMinute < 10)
        {
            randomMinuteString = "0" + randomMinute;
        }
        else
        {
            randomMinuteString = randomMinute.ToString();
        }

        print(randomHourString + " : " + randomMinuteString);
    }

    void Particles(float x, float y)
    {
        GameObject newPart = Instantiate(partSystem, new Vector3(x, y, 6f), Quaternion.identity) as GameObject;
        Destroy(newPart, 1);
    }

    public void ShowTheAnswer()
    {
        if(isMorningHour == true)
        {
            isHintShown = true;
            Particles(2f, -1.75f);
            podpowiedzGodziny.text = randomHourString;
            Particles(4.9f, -1.75f);
            podpowiedzMinuty.text = randomMinuteString;
            //TakeAPoint();
            inputHour.ActivateInputField();
        }
        else
        {
            isHintShown = true;
            Particles(2f, -1.75f);
            podpowiedzGodziny.text = randomHourStringPM;
            Particles(4.9f, -1.75f);
            podpowiedzMinuty.text = randomMinuteString;
            //TakeAPoint();
            inputHour.ActivateInputField();
        }


    }

    void GetAPoint()
    {
        ++totalPoints;
        UpdateThePoints();
        audioS.Play();
    }


    void UpdateThePoints()
    {
        ++taskNumber;
        txtTotalPoints.text = totalPoints.ToString() + " / " + taskNumber.ToString();
    }


    public void GoToWebSite()
    {
        Application.OpenURL("http://www.pikademia.pl");
    }

}
